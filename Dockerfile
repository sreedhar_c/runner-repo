FROM docker.io/gitlab/gitlab-runner
RUN curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/rpm/gitlab-runner_amd64.rpm"
RUN rpm -i gitlab-runner_amd64.rpm
RUN chmod +x /usr/local/bin/gitlab-runner
RUN useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
RUN gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
EXPOSE 80
ENTRYPOINT ["gitlab-runner start"]
